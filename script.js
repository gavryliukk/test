"use strict"
document.querySelector('.our_services_tabs_list').addEventListener('click', function (e){
    let target = e.target.closest('.our_services_tab')
    if (target){
        document.querySelector('.our_services_tab.active').classList.remove('active')
        target.classList.add('active');
        document.querySelectorAll('.our_services_tabs_text').forEach(elem => {
            elem.classList.remove('active')
            if(elem.dataset.name === target.dataset.name){
                elem.classList.add('active')
            }
        })
    }
})
let activeSecondBl = false;
let activeThirdBl = false;
document.querySelector('#work-btn').addEventListener('click', function (){
    if (!activeSecondBl && !activeThirdBl){
        document.querySelector('.loading').style.display = 'flex'
        document.querySelector('#work-btn').style.display = 'none'
        activeSecondBl = true;
        setTimeout(addImg, 1000, '.second-block');
    } else if(activeSecondBl && !activeThirdBl){
        document.querySelector('.loading').style.display = 'flex'
        document.querySelector('#work-btn').style.display = 'none'
        activeThirdBl = true
        setTimeout(addImg, 1000, '.third-block')
    }
    function addImg (block){
        document.querySelectorAll(block).forEach(element => {
            if (document.querySelector('.work-tab.active').dataset.name === element.dataset.name) element.style.display = 'block'
            else if (document.querySelector('.work-tab.active').dataset.name === 'all') element.style.display = 'block'
        })
        if(activeThirdBl){
            document.querySelector('.loading').style.display = 'none'
            document.querySelector('#work-btn').style.display = 'none'
        }else{
            document.querySelector('.loading').style.display = 'none'
            document.querySelector('#work-btn').style.display = 'flex'
        }
    }
})
document.querySelector('.work-tabs').addEventListener('click', function (e){
    let target = e.target.closest('.work-tab')
    document.querySelector('.work-tab.active').classList.remove('active')
    target.classList.add('active')
    if (!activeSecondBl && !activeThirdBl){
        select('.first-block')
        notSelect ('.second-block')
        notSelect ('.third-block')
    }
    if (activeSecondBl && !activeThirdBl){
        select('.first-block')
        select('.second-block')
        notSelect ('.third-block')
    }
    if (activeSecondBl && activeThirdBl){
        select('.first-block')
        select('.second-block')
        select('.third-block')
    }
    function select (selector){
        document.querySelectorAll(selector).forEach(e => {
            if (target.dataset.name === 'all'){
                e.style.display = 'block'
            } else if (target.dataset.name !== e.dataset.name){
                e.style.display = 'none'
            } else{
                e.style.display = 'block'
            }
        })}
    function notSelect (selector){
        document.querySelectorAll(selector).forEach(e => e.style.display = 'none')
    }
})
const aboutUsTabs = document.querySelectorAll('.about-us_tab');
const aboutUsContent = document.querySelectorAll('.about-us_comment');
let slideId = 2;
document.querySelector('.about-us_tabs-container').addEventListener('click', showTab);
const BTN_PREVIOUS = document.querySelector('.about-us_tabs-container button:first-child');
const BTN_NEXT = document.querySelector('.about-us_tabs-container button:last-child');
BTN_PREVIOUS.addEventListener('click', previousTab);
BTN_NEXT.addEventListener('click', nextTab);
function showTab(event) {
    if (event.target.tagName === 'IMG') {
        document.querySelectorAll('.about-us_tab-active').forEach(e => e.classList.remove('about-us_tab-active'));
        document.querySelectorAll('.about-us_comment-active').forEach(e => e.classList.remove('about-us_comment-active'));
        event.target.classList.add('about-us_tab-active')
        document.querySelector(event.target.getAttribute('data-id')).classList.add('about-us_comment-active')
        slideId = parseInt(event.target.getAttribute('data-index'))
    }
}
function previousTab() {
    aboutUsContent.forEach(e => e.classList.remove('about-us_comment-active'));
    aboutUsTabs.forEach(e => e.classList.remove('about-us_tab-active'));
    if (slideId === 0) {
        slideId = aboutUsTabs.length;
    }
    slideId--;
    aboutUsContent[slideId].classList.add('about-us_comment-active');
    aboutUsTabs[slideId].classList.add('about-us_tab-active');
}
function nextTab() {
    aboutUsContent.forEach(e => e.classList.remove('about-us_comment-active'));
    aboutUsTabs.forEach(e => e.classList.remove('about-us_tab-active'))
    slideId = (slideId + 1) % aboutUsTabs.length;
    aboutUsContent[slideId].classList.add('about-us_comment-active');
    aboutUsTabs[slideId].classList.add('about-us_tab-active')
}